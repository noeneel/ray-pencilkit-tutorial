//
//  RaySketchAdaptationApp.swift
//  Shared
//
//  Created by Noeneel Sharma on 2/3/21.
//

import SwiftUI

@main
struct RaySketchAdaptationApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
