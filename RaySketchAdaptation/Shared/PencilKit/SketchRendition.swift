//
//  SketchRendition.swift
//  polarity
//
//
//  Created by Noeneel Sharma on 2/3/21.
//

import Foundation
import PencilKit

struct SketchRendition {
  let title: String
  let key: String
  let drawing: PKDrawing
  let image: UIImage
}
