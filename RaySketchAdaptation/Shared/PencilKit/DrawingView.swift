//
//  DrawingView.swift
//  polarity
//
//  Created by Noeneel Sharma on 2/3/21.
//
//

import SwiftUI
import PencilKit

struct DrawingView: View {
    @Environment(\.presentationMode) var presentationMode
    @Environment(\.undoManager) var undoManager
    
    @State var rendition: SketchRendition?
    @State private var canvasView = PKCanvasView()
    
    var image: UIImage
    
    var body: some View {
            ZStack {
                CanvasView(canvasView: $canvasView, image: image, onSaved: saveDrawing)
                    .padding(20.0)
                    .background(Color.gray)
                    .navigationBarTitle(Text("Editing Mode"), displayMode: .inline)
                    .toolbar {
                        EditImageToolbar()
                        
                    }
        }.navigationViewStyle(StackNavigationViewStyle())
    }
}


// MARK: - Private Methods
private extension DrawingView {
    func saveDrawing() {
        
        // BUG: This isn't saving the background image with the drawing??
        
        let image = canvasView.drawing.image(
            from: canvasView.bounds, scale: UIScreen.main.scale)

        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        
//        let rendition = SketchRendition(
//            title: "Edited Photo",
//            key: "no-key atm",
//            drawing: canvasView.drawing,
//            image: image)
//        self.rendition = rendition
    }
    
    func deleteDrawing() {
        canvasView.drawing = PKDrawing()
    }
    
    func restoreDrawing() {
        if let rendition = rendition {
            canvasView.drawing = rendition.drawing
        }
    }
}

private extension UIImage {
    func combineDrawing(_ drawingData: PKDrawing, currentSize: CGSize) -> UIImage? {
        let drawingArea = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContext(drawingArea.size)
        draw(in: drawingArea)

        let scaleX = size.width / currentSize.width
        let scaleY = size.height / currentSize.height
        let drawing = drawingData.transformed(using: CGAffineTransform(scaleX: scaleX, y: scaleY))
        let scaledDrawingImage = drawing.image(from: drawingArea, scale: UIScreen.main.scale)
        scaledDrawingImage.draw(in: drawingArea, blendMode: .normal, alpha: 1.0)

        let outputImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return outputImage
    }
}
