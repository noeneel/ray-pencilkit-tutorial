//
//  CanvasView.swift
//  polarity
//
//  Created by Noeneel Sharma on 2/3/21.
//

import SwiftUI
import PencilKit

struct CanvasView {
    @Binding var canvasView: PKCanvasView
    
    var image: UIImage
    
    let onSaved: () -> Void
    @State var toolPicker = PKToolPicker()
}

// MARK: - UIViewRepresentable
extension CanvasView: UIViewRepresentable {
    func makeUIView(context: Context) -> PKCanvasView {
        canvasView.tool = PKInkingTool(.pen, color: .red, width: 10)
        canvasView.drawingPolicy = .anyInput
        canvasView.isOpaque = false
        canvasView.backgroundColor = UIColor.clear
        canvasView.delegate = context.coordinator
        
        // Add background image
        let targetSize = CGSize(width: 200, height: 200)
        let canvasImage = image.scalePreservingAspectRatio(targetSize: targetSize)
        let imageView = UIImageView(image: canvasImage)
        
        
        // BUG: The background image still doesn't seem
        //      to scale to fit, it's zoomed in on the canvas
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        
        
        //        let contentView = canvasView.subviews[0]
//        contentView.addSubview(imageView)
//        contentView.sendSubviewToBack(imageView)
        
        let subView = self.canvasView.subviews[0]
        subView.addSubview(imageView)
        subView.sendSubviewToBack(imageView)

        showToolPicker()
        return canvasView
    }
    
    func updateUIView(_ uiView: PKCanvasView, context: Context) {}
    
    func makeCoordinator() -> Coordinator {
        Coordinator(canvasView: $canvasView, onSaved: onSaved)
    }
    
}

// MARK: - Private Methods
private extension CanvasView {
    func showToolPicker() {
        toolPicker.setVisible(true, forFirstResponder: canvasView)
        toolPicker.addObserver(canvasView)
        canvasView.becomeFirstResponder()
    }
}

// MARK: - Coordinator
class Coordinator: NSObject {
    var canvasView: Binding<PKCanvasView>
    let onSaved: () -> Void
    
    // MARK: - Initializers
    init(canvasView: Binding<PKCanvasView>, onSaved: @escaping () -> Void) {
        self.canvasView = canvasView
        self.onSaved = onSaved
    }
}

// MARK: - PKCanvasViewDelegate
extension Coordinator: PKCanvasViewDelegate {
    func canvasViewDrawingDidChange(_ canvasView: PKCanvasView) {
        if !canvasView.drawing.bounds.isEmpty {
            onSaved()
        }
    }
}
