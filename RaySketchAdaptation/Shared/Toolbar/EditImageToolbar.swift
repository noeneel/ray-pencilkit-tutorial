//
//  EditImageToolbar.swift
//  RaySketchAdaptation (iOS)
//
//  Created by Noeneel Sharma on 2/3/21.
//

import SwiftUI

struct EditImageToolbar: ToolbarContent {
    var body: some ToolbarContent {
        ToolbarItem(placement: .confirmationAction) {
            Button(action: {
                // Do something
            }) {
                Text("Save")
                    .accessibilityLabel("Done")
                    .buttonStyle(PlainButtonStyle())
            }
        }

        ToolbarItem(placement: .cancellationAction) {
            Button(action: {
                // Do something
            }) {
                Text("Cancel")
                    .foregroundColor(Color(UIColor.systemRed))
                    .accessibilityLabel("Cancel")
                    .buttonStyle(PlainButtonStyle())
            }
        }

        
        // Doesn't work from here
        ToolbarItem(placement: .cancellationAction) {
            Button(action: {
                print("Undo Hit")
//                undoManager?.undo()
            }) {
                Image(systemName: "arrow.uturn.backward.circle")
                    .accessibilityLabel("Undo Edit")
                    .buttonStyle(PlainButtonStyle())
            }
        }

        ToolbarItem(placement: .cancellationAction) {
            Button(action: {
                print("Redo Hit")
//                undoManager?.redo()
            }) {
                Image(systemName: "arrow.uturn.forward.circle")
                    .accessibilityLabel("Redo Edit")
                    .buttonStyle(PlainButtonStyle())
            }
        }

        ToolbarItem(placement: .destructiveAction) {
            Button(action: {
                // Do something
            }) {
                Image(systemName: "trash")
                    .accessibilityLabel("Delete Photo")
                    .buttonStyle(PlainButtonStyle())
            }
        }
    }
}
